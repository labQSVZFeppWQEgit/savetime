using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;


class Program
{
    static void Main(string[] args)
    {

	//Regex rx = new Regex(@"^[\.a-Z0-9]+@", RegexOptions.Compiled);
	Regex rx = new Regex(@"^.+\.(html|jpg|js)$", RegexOptions.Compiled);

        try
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:1000/");
            listener.Start();
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerResponse res = context.Response;
                res.StatusCode = 200;

		if (context.Request.Url.LocalPath == "/")
		{
			TextReader textreader = new StreamReader("index.html");
			string msg = textreader.ReadToEnd();
			byte[] bytes = Encoding.UTF8.GetBytes(msg);
			res.OutputStream.Write(bytes, 0, bytes.Length);
			textreader.Close();
			
		} 
		else if (context.Request.Url.LocalPath == "/add")
		{

			//System.Web.HttpFileCollection files;

			//System.Net.HttpListenerRequest rq = context.Request;
			//System.Web.HttpPostedFile file = rq.Files.Item(0);
	
			StreamReader post_reader = new StreamReader(context.Request.InputStream);

			System.Text.Encoding enc = context.Request.ContentEncoding;

			string v = post_reader.ReadToEnd();
			Console.WriteLine("xx START xx");
			Console.WriteLine(v);
			Console.WriteLine("xx END xx");

			JavaScriptSerializer szr = new JavaScriptSerializer(); 
			Dictionary<string, string>ppp = szr.Deserialize<Dictionary<string, string>>(v);
			foreach(string key in ppp.Keys)
			{
				Console.WriteLine(key);
				Console.WriteLine(ppp[key]);
				MyEntry.add(key + ":" + ppp[key]);
				
			}
			
			

//System.Collections.Specialized.NameValueCollection namePair
//= HttpUtility.ParseQueryString(v, enc);

			//string post_values = post_reader.ReadToEnd();
			//Console.WriteLine(post_values);

			//Console.WriteLine(context.Request.current.Form['a']);
			//Console.WriteLine(context.Request.Form("a1name"));
			//Console.WriteLine("----");

			//post_reader.Close();

//			Console.WriteLine("--1--");
//			foreach(string s in namePair.AllKeys) {
//				Console.WriteLine(s);
//				Console.WriteLine(namePair.GetValues(s));
//			}
//			Console.WriteLine("--2--");


			//jHttpListenerRequest req = context.Request;
			//System.IO.Stream body = req.InputStream;
			//System.Text.Encoding enc = req.ContentEncoding;

			//System.IO.StreamReader reader = new System.IO.StreamReader(body, enc);
	
			//string s = reader.ReadToEnd();
			//Console.Write(s);
			//body.Close();
			//reader.Close();

			//string[] keys = context.Request.Form.AllKeys;
			//for(int i=0;i< keys.Length;i++) {
				//MyEntry.add(keys[i] + ":" + context.Request.Form[keys[i]]);
			//}	

			Console.Write("end***");


		} 
		else if (context.Request.Url.LocalPath.Length > 0 & 
				rx.IsMatch(context.Request.Url.LocalPath))
		{
			String path = context.Request.Url.LocalPath.Substring(1);
			Console.Write(path);

			if (System.IO.File.Exists(path)) {
				TextReader textreader = new StreamReader(path);
				string msg = textreader.ReadToEnd();
				byte[] bytes = Encoding.UTF8.GetBytes(msg);
				res.OutputStream.Write(bytes, 0, bytes.Length);
				textreader.Close();
			} else {
                		byte[] content = Encoding.UTF8.GetBytes("not found");
                		res.OutputStream.Write(content, 0, content.Length);
			}

		} else {
                	byte[] content = Encoding.UTF8.GetBytes("Welcome!");
                	res.OutputStream.Write(content, 0, content.Length);
		}
                res.Close();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message);
        }
    }
}

